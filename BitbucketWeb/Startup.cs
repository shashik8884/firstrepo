﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BitbucketWeb.Startup))]
namespace BitbucketWeb
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
